<?php


namespace App\Resources;


use App\Resources\Exceptions\InvalidRequestException;

class Validations
{

    /**
     * @param string $json
     * @return object
     * @throws InvalidRequestException
     */
    public static function validateCreateUser(string $json)
    {
        $json = json_decode($json, true);
        $jsonObject = (Object) $json;

        if (!property_exists($jsonObject, "name") || !is_string($jsonObject->name)) {
            throw new InvalidRequestException("Missing parameter name", 400);
        }

        if (!property_exists($jsonObject, "lastName") || !is_string($jsonObject->lastName)) {
            throw new InvalidRequestException("Missing parameter lastName", 400);
        }

        if (!property_exists($jsonObject, "phone") || !is_numeric($jsonObject->phone)) {
            throw new InvalidRequestException("Missing parameter phone", 400);
        }

        if (!property_exists($jsonObject, "dni") || !is_string($jsonObject->phone)) {
            throw new InvalidRequestException("Missing parameter dni", 400);
        }

        if (!property_exists($jsonObject, "username") || !is_string($jsonObject->username)) {
            throw new InvalidRequestException("Missing parameter username", 400);
        }

        if (!property_exists($jsonObject, "password") || !is_string($jsonObject->password)) {
            throw new InvalidRequestException("Missing parameter password", 400);
        }

        if (!property_exists($jsonObject, "bank") || !is_int($jsonObject->bank)) {
            throw new InvalidRequestException("Missing parameter bank", 400);
        }

        return $jsonObject;
    }


    /**
     * @param string $json
     * @return object
     * @throws InvalidRequestException
     */
    public static function validateTransfer(string $json)
    {
        $json = json_decode($json, true);
        $jsonObject = (Object) $json;

        if (!property_exists($jsonObject, "bank") || !is_int($jsonObject->bank)) {
            throw new InvalidRequestException("Missing parameter bank", 400);
        }

        if (!property_exists($jsonObject, "phone") || !is_numeric($jsonObject->phone)) {
            throw new InvalidRequestException("Missing parameter phone", 400);
        }

        if (!property_exists($jsonObject, "dni") || !is_string($jsonObject->phone)) {
            throw new InvalidRequestException("Missing parameter dni", 400);
        }

        if (!property_exists($jsonObject, "amount") || !is_double($jsonObject->amount)) {
            throw new InvalidRequestException("Missing parameter amount", 400);
        }

        return $jsonObject;
    }
}