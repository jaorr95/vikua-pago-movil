<?php


namespace App\Resources;


use Symfony\Component\HttpFoundation\Response;

class Utils
{

    /**
     * @param array $object
     * @param string $message
     * @return array
     */
    public static function response(array $object = [], string $message = "Request succesfull")
    {

        $response = [

            "data" => $object,
            "message" => $message
        ];

        return $response;

    }

}