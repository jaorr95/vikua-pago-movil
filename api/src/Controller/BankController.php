<?php


namespace App\Controller;


use App\Bussines\BankBo;
use App\Resources\Utils;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class BankController extends AbstractController
{

    /**
     * @Route("/api/bank/index", methods={"GET"})
     * @param BankBo $bankBo
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function index(BankBo $bankBo)
    {
        $banks = $bankBo->getAll();
        return $this->json(Utils::response($banks));
    }

}