<?php


namespace App\Controller;


use App\Bussines\UserBo;
use App\Resources\Exceptions\InvalidRequestException;
use App\Resources\Utils;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserController extends AbstractController
{

    /**
     * @Route("/api/user/create", methods={"POST"})
     * @param Request $request
     * @param UserBo $userBo
     * @param UserPasswordEncoderInterface $encoder
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function register(Request $request, UserBo $userBo, UserPasswordEncoderInterface $encoder)
    {

        $params = $request->getContent();
        try {

            $userBo->createUser($params, $encoder);
            return $this->json(Utils::response());

        } catch (InvalidRequestException $ex) {
            return $this->json(Utils::response([], $ex->getMessage()), $ex->getCode());
        }

    }

    /**
     * @Route("/api/secure/user/balance", methods={"GET"})
     * @param UserBo $userBo
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function balance(UserBo $userBo)
    {
        $balance = $userBo->getBalance($this->getUser());
        return $this->json(Utils::response($balance));
    }

    /**
     * @Route("/api/secure/user/transfer", methods={"POST"})
     * @param Request $request
     * @param UserBo $userBo
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function transfer(Request $request, UserBo $userBo)
    {
        $params = $request->getContent();
        try {

            $userBo->transfer($params, $this->getUser());
            return $this->json(Utils::response());

        } catch (InvalidRequestException $ex) {
            return $this->json(Utils::response([], $ex->getMessage()), $ex->getCode());
        }
    }

}