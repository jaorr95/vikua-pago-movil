<?php


namespace App\Entity\Factory;


use App\Entity\Account;
use App\Entity\Bank;
use App\Entity\User;

class EntityFactory
{
    /**
     * @param string $name
     * @param string $lastName
     * @param string $username
     * @param string $password
     * @param int $phone
     * @param Bank $bank
     * @param string $dni
     * @return User
     */
    public static function getUser(string $name, string $lastName, string $username,
                                   string $password, int $phone, Bank $bank, string $dni)
    {
        $user = new User();
        $user->setName($name)->setLastName($lastName)->setPassword($password)->setPhone($phone)->setUsername($username)
            ->setBank($bank)->setDni($dni);

        return $user;
    }

    /**
     * @param string $number
     * @param float $amount
     * @return Account
     */
    public static function getAccount(string $number, float $amount)
    {
        $account = new Account();
        $account->setNumber($number)->setAmount($amount);
        return $account;
    }

}