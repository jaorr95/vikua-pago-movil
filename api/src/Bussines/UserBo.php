<?php


namespace App\Bussines;


use App\Entity\Bank;
use App\Entity\Factory\EntityFactory;
use App\Entity\User;
use App\Resources\Exceptions\InvalidRequestException;
use App\Resources\Validations;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserBo
{
    /** @var EntityManagerInterface */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }


    /**
     * @param string $json
     * @param UserPasswordEncoderInterface $encoder
     * @throws InvalidRequestException
     */
    public function createUser(string $json, UserPasswordEncoderInterface $encoder)
    {
        $data = Validations::validateCreateUser($json);

        /** @var Bank $bank */
        $bank = $this->em->getRepository(Bank::class)->findOneBy(['id' => $data->bank]);

        if (!$bank) {
            throw new InvalidRequestException("Invalid bank", 400);
        }

        $this->uniquePhoneAndBank($data->phone, $bank);


        $user = EntityFactory::getUser($data->name, $data->lastName, $data->username,
            $data->password, $data->phone, $bank, $data->dni);

        $encoded = $encoder->encodePassword($user, $user->getPassword());
        $user->setPassword($encoded);

        $account = EntityFactory::getAccount(uniqid(), 1000000.0);
        $account->setUser($user);

        $this->em->persist($account);
        $this->em->flush();

    }

    /**
     * @param int $phone
     * @param Bank $bank
     * @throws InvalidRequestException
     */
    private function uniquePhoneAndBank(int $phone, Bank $bank)
    {
        $user = $this->em->getRepository(User::class)->findOneBy(["phone" => $phone, "bank" => $bank]);
        if ($user) {
            throw new InvalidRequestException("The user with this phone and bank already exist", 400);
        }

    }

    /**
     * @param User $user
     * @return array
     */
    public function getBalance(User $user)
    {
        $response = new \stdClass();
        $response->name = $user->getName();
        $response->lastName = $user->getLastName();
        $response->bank = $user->getBank()->getName();
        $response->amount = $user->getAccounts()[0]->getAmount();
        return [$response];
    }

    /**
     * @param string $json
     * @param User $user
     * @throws InvalidRequestException
     */
    public function transfer(string $json, User $user)
    {
        $data = Validations::validateTransfer($json);

        /** @var Bank $bank */
        $bank = $this->em->getRepository(Bank::class)->findOneBy(['id' => $data->bank]);

        if (!$bank) {
            throw new InvalidRequestException("Invalid bank", 400);
        }

        /** @var User $toUser */
        $toUser = $this->em->getRepository(User::class)->findOneBy(
            ["phone" => $data->phone, "bank" => $bank, "dni" => $data->dni]
        );

        if (!$toUser || $toUser->getId() == $user->getId()) {
            throw new InvalidRequestException("User to send transfer does not exist", 400);
        }

        if ($data->amount > $user->getAccounts()[0]->getAmount()) {
            throw new InvalidRequestException("User does not has enough money", 400);
        }

        $toUser->getAccounts()[0]->setAmount($data->amount + $toUser->getAccounts()[0]->getAmount());
        $user->getAccounts()[0]->setAmount($user->getAccounts()[0]->getAmount() - $data->amount);

        $this->em->flush();


    }
}