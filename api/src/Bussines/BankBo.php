<?php


namespace App\Bussines;


use App\Entity\Bank;
use Doctrine\ORM\EntityManagerInterface;

class BankBo
{

    /** @var EntityManagerInterface */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @return array
     */
    public function getAll()
    {
        $banks = $this->em->getRepository(Bank::class)->findAll();
        return $this->bankToResponse(...$banks);
    }

    /**
     * @param Bank ...$banks
     * @return array
     */
    public function bankToResponse(Bank ...$banks)
    {
        $array = [];
        foreach ($banks as $bank) {
            $newElem = new \stdClass();

            $newElem->id = $bank->getId();
            $newElem->name = $bank->getName();

            $array[] = $newElem;

        }

        return $array;
    }



}